﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESPad.Models
{
    public class StickyNotes
    {
        public Guid StickyNoteId { get; set; }
        public Guid UserId { get; set; }
        public string NoteDescription { get; set; }
        public int NotePriority { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
