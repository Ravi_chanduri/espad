﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESPad.Models.Main
{
    public class MyNotes
    {
        public Guid NoteID { get; set; }
        public Guid UserID { get; set; }
        public DateTime NoteDate { get; set; }
        public string Notes { get; set; }
    }
}
