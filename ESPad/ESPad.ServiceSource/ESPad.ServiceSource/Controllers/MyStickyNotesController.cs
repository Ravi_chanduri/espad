﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ESPad.Models;
using ESPad.Database;
namespace ESPad.ServiceSource.Controllers
{
    public class MyStickyNotesController : Controller
    {
        private Guid UserID = new Guid("9C7C8273-E7F4-44B1-8922-4EDBCE8F36F9");//it wll removed once authentication has been implementec

        public ActionResult GetMyStickynotes()
        {
            List<StickyNotes> allNotes = new List<StickyNotes>();
            allNotes = new StickyNotesDBHandler().GetStickyNotes(UserID);
            return View(allNotes);
        }
    }
}