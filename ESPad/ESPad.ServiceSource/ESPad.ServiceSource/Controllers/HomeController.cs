﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using ESPad.Models.Main;

namespace ESPad.ServiceSource.Controllers
{
    public class HomeController : Controller
    {
        private Guid UserID =new Guid( "9C7C8273-E7F4-44B1-8922-4EDBCE8F36F9");//it wll removed once authentication has been implementec

        public ActionResult Index()
        {
            ViewBag.Title = "Today Notes";
            MyNotes myNotes = new MyNotes();
            using (var Client = new HttpClient())
            {
                Client.BaseAddress = new Uri("http://localhost:53233/api/");
                var responseTask = Client.GetAsync("MyNotes/GetMyNotes/"+ UserID+ "/false");
                responseTask.Wait();
                var results = responseTask.Result;
                if (results.IsSuccessStatusCode)
                {
                    var readTask = results.Content.ReadAsAsync<MyNotes>();
                    readTask.Wait();
                    myNotes = readTask.Result;
                }
                else
                {
                    ModelState.AddModelError("", "Facing issues while fetching your Notes , Please contact Ravi-(+91 - 9700700042)");
                }
            }
            return View(myNotes);
        }
        public ActionResult SaveNotes(MyNotes myNotes)
        {
            using (var Client = new HttpClient())
            {
                Client.BaseAddress = new Uri("http://localhost:53233/api/");
                //var postTask = Client.GetAsync("MyNotes/UpdateMyNote");
                var postTask = Client.PostAsJsonAsync<MyNotes>("MyNotes/UpdateMyNote", myNotes);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return RedirectToAction("Index");

        }
    }
}
