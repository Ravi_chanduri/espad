﻿using ESPad.Models.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ESPad.ServiceSource.Controllers
{
    public class MyNotesController : ESPadBaseController
    {
        //todo: use DB context to get data for my notes
        // create a UI to show My notes in MVC 
        //add proper routs for actionmethods where it is required

        [HttpGet]
        public MyNotes GetMyNotes()
        {
            return myNotesDBHandlers.GetMyNotes();
        }
        [HttpGet]
        [Route("api/MyNotes/GetMyNotes/{UserId}/{IsForSelection}")]
        //public MyNotes GetMyNotes(Guid UserId, DateTime NoteDate)
        public MyNotes GetMyNotes(Guid UserId,bool IsForSelection)
        {
            return myNotesDBHandlers.GetMyNotes(UserId, DateTime.Now.ToString("MM-dd-yyy"),IsForSelection);
        }
        [HttpGet]
        [Route("api/MyNotes/GetMyNotesByDate/{UserId}/{Notedate}")]
        //public MyNotes GetMyNotes(Guid UserId, DateTime NoteDate)
        public MyNotes GetMyNotesByDate(Guid UserId, string Notedate)
        {
            return myNotesDBHandlers.GetMyNotes(UserId, Notedate, true);
        }
        [HttpPost]
        //public int UpdateMyNote(Guid NoteId, string Notes)
        public int UpdateMyNote(MyNotes Notes)
        {
            return myNotesDBHandlers.UpdateMyNote(Notes);
        }
        [HttpGet]
        [Route("api/MyNotes/GetFullName/{firstName}/{lastName}")]
        [Authorize]
        public string GetFullName(string firstName,string lastName)
        {
            return firstName + lastName;
        }

    }
}
