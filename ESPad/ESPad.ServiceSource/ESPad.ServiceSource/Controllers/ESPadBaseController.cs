﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using ESPad.Database;

namespace ESPad.ServiceSource.Controllers
{
    public abstract class ESPadBaseController : ApiController
    {
        public MyNotesDBHandlers myNotesDBHandlers;
        public ESPadBaseController()
        {
            myNotesDBHandlers = new MyNotesDBHandlers();
        }
    }
}