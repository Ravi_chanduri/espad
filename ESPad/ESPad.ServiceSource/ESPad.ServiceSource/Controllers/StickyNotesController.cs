﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ESPad.Models;
using ESPad.Database;
namespace ESPad.ServiceSource.Controllers
{
    public class StickyNotesController : ESPadBaseController
    {
        StickyNotesDBHandler stickyNotesDBHandler = new StickyNotesDBHandler();
        [HttpGet]
        public List<StickyNotes> GetStickyNotes(Guid UserID)
        {
            return stickyNotesDBHandler.GetStickyNotes(UserID);
        }
        [HttpPost]
        public StickyNotes InsertStickyNote(Guid UserId)
        {
            return stickyNotesDBHandler.InserStickyNote(UserId);
        }
        [HttpPost]
        public StickyNotes UpdateStickyNotes(StickyNotes stickyNotes)
        {
            return stickyNotesDBHandler.UpdateStickyNote(stickyNotes);
        }
    }
}
