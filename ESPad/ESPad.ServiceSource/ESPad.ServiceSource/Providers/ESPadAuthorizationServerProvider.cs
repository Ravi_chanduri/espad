﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ESPad.Database;
using ESPad.Models;
using System.Security.Claims;

namespace ESPad.ServiceSource.Providers
{
    public class ESPadAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            using (DBContext userDBHandler = new DBContext())
            {
                User user = userDBHandler.GetUserByID(context.UserName, context.Password);
                if (user.UniqueID != context.UserName)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

            }
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));

            context.Validated(identity);
        }

    }
}