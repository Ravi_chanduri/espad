﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESPad.Models;
namespace ESPad.Database
{
    public class UserDBHandler
    {
        public User GetUserByID(string UniqueId,string Password)
        {
            dynamic result = null;
            using (DBContext DBC = new DBContext())
            {
                result = DBC.GetUserByID(UniqueId,Password);
            }
            return result;
        }
    }
}
