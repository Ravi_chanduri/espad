﻿using ESPad.Models.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESPad.Database
{
    public class MyNotesDBHandlers
    {
        public MyNotes GetMyNotes()
        {
            dynamic result = null;
            using (DBContext DBC = new DBContext())
            {
                result = DBC.GetData();
            }
            return result;
        }
        public MyNotes GetMyNotes(Guid UserId, string NoteDate,bool IsForSelection)
        {
            dynamic result = null;
            using (DBContext DBC = new DBContext())
            {
                result = DBC.GetData(UserId,NoteDate, IsForSelection);
            }
            return result;
        }
        //it should call after inserting the note 
        // update alwasy based on the UserID and current date or NoteGuid
        //Todo: create SP to update the note
        public int UpdateMyNote(MyNotes myNotes)
        {
            try
            {
                using (DBContext DBC = new DBContext())
                {
                     DBC.UpdateNotes(myNotes);
                }
                return 1;
            }
            catch(Exception e)
            {
                return 0;
            }

        }
        //it should call only once per day per user
        //Todo: create SP to insert the note
        //public Guid InsertMyNote(Guid UserId, string Notes, DateTime NoteDate)
        //{
        //    Guid NoteGuid;
        //    using (DBContext DBC = new DBContext())
        //    {
        //        NoteGuid = DBC.InsertNotes(UserId, Notes, NoteDate);
        //    }
        //    return NoteGuid;
        //}
    }
}

