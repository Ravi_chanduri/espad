﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ESPad.Database
{
    public class DBConnectionManager
    {
        public string GetconnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyNotesLogin"].ToString();
        }
    }
}
