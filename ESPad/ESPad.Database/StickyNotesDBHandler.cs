﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESPad.Models;
namespace ESPad.Database
{
    public class StickyNotesDBHandler
    {
        DBContext dBContext;
        public void StickyNotes()
        {
            dBContext = new DBContext();
        }
        public List<StickyNotes> GetStickyNotes(Guid UserID)
        {
            dBContext = new DBContext();
            return dBContext.GetAllStickyNotesByUserId(UserID).ToList<StickyNotes>();
        }
        public StickyNotes InserStickyNote( Guid UserId)
        {
            dBContext = new DBContext();
            return dBContext.CreateStickyNoteForUser(UserId);
        }
        //below method will handle both update and delete
        public StickyNotes UpdateStickyNote(StickyNotes stickyNotes)
        {
            dBContext = new DBContext();
            return dBContext.UpdateStickyNoteForUser(stickyNotes);
        }
    }
}
