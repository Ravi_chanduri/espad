﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESPad.Models.Main;
using System.Data;
using System.Data.SqlClient;
using ESPad.Models;
using ESPad.Database.ExtensionMethods;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;

namespace ESPad.Database
{
    //todo: naming conventions
    public class DBContext : IDisposable
    {
        SqlConnection connection = null;
        public DBContext()
        {
            //todo: implement dependancy injection to set the DB
            connection = new SqlConnection(new DBConnectionManager().GetconnectionString());
        }
        // Flag: Has Dispose already been called?
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
            }

            disposed = true;
        }

        public void ExecuteNonQuery(string commandText, CommandType commandType, params SqlParameter[] commandParameters)
        {
            using (var command = new SqlCommand(commandText, connection))
            {
                command.CommandType = commandType;
                command.Parameters.AddRange(commandParameters);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
        public void ExecuteScalar()
        {

        }
        //todo: Convert ds to my notes
        //make this method as generic method
        //create a new method in utilities class for convert ds to class types.
        //Create table/entity class for entity level operations.(for eg: all operations related to My notes should go through MyNotesDBHandler.cs
        //send the UserID to get related notes
        //need to make this method generic for all other model classes
        public User GetUserByID(string UniqueId, string Password)
        {
            SqlCommand command = new SqlCommand(string.Format("select top 1 * from UserData where [UniqueID]='{0}' and [UserPassword]='{1}'", UniqueId, Password), connection);

            command.CommandType = CommandType.Text;
            //command.Parameters.AddRange(sqlParameters);
            connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].ToList<User>()[0] : new User();
        }
        public MyNotes GetData()
        {
            SqlCommand command = new SqlCommand("select top 1 * from MyNotes", connection);
            connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds.Tables[0].ToList<MyNotes>()[0];
        }
        public MyNotes GetData(Guid UserId, string NoteDate, bool IsForSelection)
        {
            string CommandText = string.Format("GetMyNotes");
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@UserId", "9C7C8273-E7F4-44B1-8922-4EDBCE8F36F9"));
            sqlParameters.Add(new SqlParameter("@Date", NoteDate));
            sqlParameters.Add(new SqlParameter("@IsForSelection", IsForSelection));
            return GetNotesData(CommandText, sqlParameters.ToArray());
        }
        public void UpdateNotes(MyNotes myNotes)
        {
            string CommandText = string.Format("update  MyNotes set Notes={0} where NoteID={1}", "'" + myNotes.Notes + "'", "'" + myNotes.NoteID + "'");
            UpdateNotesData(CommandText);
        }
        public void InsertNotes(Guid UserId, string Notes, DateTime NoteDate)
        {
            Guid guid = new Guid();
            string CommandText = string.Format("Inser Into table MyNotes values({0},{1},{2},{3})", guid, UserId, NoteDate.ToShortDateString(), Notes);
            //GetNotesData(CommandText);
            //return guid;
        }
        private MyNotes GetNotesData(string Command, SqlParameter[] sqlParameters)
        {
            MyNotes notes = new MyNotes();
            SqlCommand command = new SqlCommand(Command, connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(sqlParameters);
            connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                notes = ds.Tables[0].ToList<MyNotes>()[0];
            }
            return notes;
        }
        private int UpdateNotesData(string Command)
        {
            SqlCommand command = new SqlCommand(Command, connection);
            connection.Open();
            return command.ExecuteNonQuery();
        }

        #region sticky notes section
        public IList<StickyNotes> GetAllStickyNotesByUserId(Guid UserId)
        {
            string CommandText = string.Format("select * from MyStickyNotes where UserId ='"+ UserId + "'");
            SqlCommand command = new SqlCommand(CommandText, connection);
            command.CommandType = CommandType.Text;
            connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds.Tables[0].ToList<StickyNotes>();
        }
        public StickyNotes CreateStickyNoteForUser(Guid UserId)
        {
            Guid guid = Guid.NewGuid();
            DateTime dateTime = DateTime.Now;
            string CommandText = string.Format("insert into MyStickyNotes values('"+ guid + "','" + UserId+"','',3,'"+ dateTime + "','" + dateTime + "')");
            SqlCommand command = new SqlCommand(CommandText, connection);
            command.CommandType = CommandType.Text;
            connection.Open();
            int result=command.ExecuteNonQuery();
            if (result == 1)
            {
                return new StickyNotes()
                {
                    StickyNoteId = guid,
                    UserId = UserId,
                    NotePriority = 3,
                    NoteDescription = "",
                    AddedOn = dateTime,
                    ModifiedOn = dateTime

                };
            }
            else
                return new StickyNotes();
        }
        /// <summary>
        ///  for update and delete
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public StickyNotes UpdateStickyNoteForUser(StickyNotes stickyNotes)
        {
            Guid guid = Guid.NewGuid();
            DateTime dateTime = DateTime.Now;
            string CommandText = string.Format("update  MyStickyNotes set NoteDescription='"+ stickyNotes .NoteDescription+ "',NotePriority='"+stickyNotes.NotePriority+ "',ModifiedOn='" + dateTime + "' where StickyNoteId=  '" + stickyNotes + "')");
            SqlCommand command = new SqlCommand(CommandText, connection);
            command.CommandType = CommandType.Text;
            connection.Open();
            int result = command.ExecuteNonQuery();
            if (result == 1)
            {
                return stickyNotes;
            }
            else
                return new StickyNotes();
        }

        #endregion
    }
}
